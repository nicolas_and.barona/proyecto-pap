<?php

namespace App\Http\Controllers;
use App\Usuario;
use Illuminate\Http\Request;
use Hash;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios=Usuario::where('rol_id', 2)->get();
        return view('./admin_modulos/meditor', compact('usuarios'));
    }

    public function indexE()
    {
        $usuarios=Usuario::where('rol_id', 2)->get();
         return view('./admin_modulos/asignareditor', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['nombres']=$request->get('nombres');
        $data['apellidos']=$request->get('apellidos');
        $data['password'] = Hash::make( $data['password'] );
        $data['email']=$request->get('email');
        $data['telefono']=$request->get('telefono');
        $data['direccion']=$request->get('direccion');
        $data['tipoDocumento_id']=$request->get('tipoDocumento_id');
        $data['documento']=$request->get('documento');
        $data['rol_id']=3;

        Usuario::create( $data );

        return redirect('login');
    }

    public function RegistroEditor(Request $request)
    {
        $data = $request->all();
        $data['nombres']=$request->get('nombres');
        $data['apellidos']=$request->get('apellidos');
        $data['password'] = Hash::make( $data['password'] );
        $data['email']=$request->get('email');
        $data['telefono']=$request->get('telefono');
        $data['direccion']=$request->get('direccion');
        $data['tipoDocumento_id']=$request->get('tipoDocumento_id');
        $data['documento']=$request->get('documento');
        $data['rol_id']=2;

        Usuario::create( $data );

        return redirect('meditor');
    }

     public function RegistroCliente(Request $request)
    {
        $data = $request->all();
        $data['nombres']=$request->get('nombres');
        $data['apellidos']=$request->get('apellidos');
        $data['password'] = Hash::make( $data['password'] );
        $data['email']=$request->get('email');
        $data['telefono']=$request->get('telefono');
        $data['direccion']=$request->get('direccion');
        $data['tipoDocumento_id']=$request->get('tipoDocumento_id');
        $data['documento']=$request->get('documento');
        $data['rol_id']=3;

        Usuario::create( $data );

        return redirect('meditor');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit($usuario)
    {
        $usuarios = Usuario::where('id', $usuario)->get();
        $usuarios = $usuarios[0];
        return view('./admin_modulos/editarEditor', compact('usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $usuario)
    {

        $data['nombres']=$request->get('nombres');
        $data['apellidos']=$request->get('apellidos');
        $data['email']=$request->get('email');
        //$data['user']=$request->get('user');
        $data['telefono']=$request->get('telefono');
        $data['direccion']=$request->get('direccion');
        //$data['tipoDocumento_id']=$request->get('tipoDocumento_id');
        //$data['documento']=$request->get('documento');

        Usuario::where('id',$usuario)->update($data);
        
        return redirect('meditor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($usuario)
    {
        Usuario::where('id',$usuario)->delete();
        return redirect('meditor');
    }
}
