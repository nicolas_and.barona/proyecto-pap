<?php

namespace App\Http\Controllers;

use App\Ordene;
use App\Usuario;
use Auth;
use Illuminate\Http\Request;

class OrdeneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ordenes=Ordene::with('artista')->get();
        return view('./admin_modulos/mcliente', compact('ordenes'));
         
    }
    public function indexV()
    {
        $ordenes=Ordene::with('artista')->get();
        //dd($ordenes[0]->artista->nombres);
        return view('./admin_modulos/verOrden', compact('ordenes'));
         
    }
    public function indexO()
    {
        $ordenes=Ordene::where('usuario_id', Auth::id())->get();
        //dd($ordenes[0]->artista->nombres);
        return view('verOrdenC', compact('ordenes'));
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        $file = $request->file('imagen');
        
        $path = public_path('pics');

        $name = $file->getClientOriginalName();
        
        if( $file->move($path,  $name) ){
            $data['imagen'] = $name;
            $data['estado_id']=1;
            $data['usuario_id']=Auth::id();
            Ordene::create( $data );
            
        }

        return back();

        /* $data=$request->all();
        
        $data['estado_id']=1;
        $data['usuario_id']=Auth::id();
        
        Ordene::create( $data );

        return back(); */

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ordene  $ordene
     * @return \Illuminate\Http\Response
     */
    public function show(Ordene $ordene)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ordene  $ordene
     * @return \Illuminate\Http\Response
     */
    public function edit($ordene)
    {
        $ordenes = Ordene::where('id', $ordene)->get();
        $ordenes = $ordenes[0];
        $usuarios=Usuario::where('rol_id', 2)->get();
        //dd($ordenes);
        return view('./admin_modulos/asignareditor', compact('usuarios', 'ordenes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ordene  $ordene
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ordene)
    {
        $data['artista_id']=$request->get('artista_id');
        $data['estado_id']=$request->get('estado_id');

        Ordene::where('id',$ordene)->update($data);
        
        return redirect('mcliente');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ordene  $ordene
     * @return \Illuminate\Http\Response
     */
    public function destroy($ordene)
    {
        Ordene::where('id',$ordene)->delete();
        return redirect('mcliente');
    }
}
