<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
   
    public function check(Request $request)
    {
        $data = $request->only('email', 'password');

        if (Auth::attempt($data))
            if(Auth::user()->rol_id==1){
                return redirect()->intended('admin');
            }else if(Auth::user()->rol_id==2){
                return redirect()->intended('editor');
            }else{
                return redirect()->intended('cliente');
            }
           
        
        return redirect('login')->with('message', 'Datos incorrectos!');
    }


    public function logout()
    {
        Auth::logout();
        return redirect('login');   
    }

}
