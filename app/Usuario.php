<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable; 
use Illuminate\Database\Eloquent\Model;

class Usuario extends Authenticatable 
{
    protected $fillable=['nombres','apellidos','user','password','email','email_verified_at','telefono','direccion','rol_id',
                         'tipoDocumento_id','documento','remember_token'];

    public function roles(){
        return $this->belongsTo('App\Role','rol_id');
    }
    public function documentos(){
        return $this->belongsTo('App\Documento','tipoDocumento_id');
    }
}
