<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordene extends Model
{
    protected $fillable=['estado_id','usuario_id','descripcion','calificacion',
                         'producto_id','imagen','artista_id','valor'];

    public function estado(){
        return $this->belongsTo('App\Estado','estado_id');
    }
    /* public function usuario(){
        return $this->hasMany('App\Usuario','usuario_id');
    } */
    public function usuario(){
        return $this->belongsTo('App\Usuario','usuario_id');
    }
    public function producto(){
        return $this->belongsTo('App\Producto','producto_id');
    }
    /* public function artista(){
        return $this->hasMany('App\Usuario','artista_id');
    } */
    public function artista(){
        return $this->belongsTo('App\Usuario','artista_id');
    }
}
