<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ url('../public/img/logo.png') }}" type="image/ico" />

    <title>Photo Artistas Pro | </title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('../public/vendors/bootstrap/dist/css/bootstrap.min.css') }}" >
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('../public/vendors/font-awesome/css/font-awesome.min.css') }}" >
    <!-- NProgress -->
    <link rel="stylesheet" href="{{ url('../public/vendors/nprogress/nprogress.css') }}" >
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ url('../public/vendors/animate.css/animate.min.css') }}" >

    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="{{ url('../public/build/css/custom.min.css') }}" >
  </head>

  <body class="login">
    <div>

      <div class="login_wrapper">
        <div class="animate form login_form">
        <section class="login_content">
            <form action="{{ url('guardar') }}" method="post">
               @csrf
              <img src="{{ url('../public/img/logo.png') }}" width="110" height="90">
              <h1>Crea una cuenta con PAP!</h1>
              <div>
                <input type="text" class="form-control" placeholder="Nombres" required="" name="nombres"/>
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Apellidos" required="" name="apellidos" />
              </div>
              <div>
                <select class="form-control" require="" name="tipoDocumento_id">
                  @foreach($documentos as $documento)
                  <option value="{{$documento->id}}">{{$documento->nombre}}</option>
                  @endforeach
                </select>
                <br>
              </div>
              <div>
                <input type="number" class="form-control" placeholder="Número de identificacion" required="" name="documento" />
                <br>
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" name="email" />
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Telefono" required="" name="telefono" />
              </div>"
              <div>
                <input type="text" class="form-control" placeholder="Dirección" required="" name="direccion" />
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Usuario" required="" name="user" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Contraseña" required="" name="password" />
              </div>
              <div>
              <button type="submit" class="btn btn-primary btn-block">Registrar</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Ya tienes cuenta con nostros ?
                  <a href="login" class="to_register"> Inicia sesión </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
