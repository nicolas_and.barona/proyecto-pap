<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('login', function () {
    return view('login');
})->name('login');

Route::get('register', 'DocumentoController@index');

Route::post('guardar', 'UsuarioController@store');
Route::post('check', 'LoginController@check');
Route::get('logout', 'LoginController@logout'); 

Route::middleware(['auth'])->group(function () {

    Route::get('admin', function () {
        return view('admin');
    });
    Route::get('cliente', function () {
        return view('cliente');
    });
    Route::get('editor', function () {
        return view('editor');
    });
    Route::get('mcliente', function () {
        return view('./admin_modulos/mcliente');
    });
    Route::get('meditor', function () {
        return view('./admin_modulos/meditor');
    });
    Route::get('registrarEditor', function () {
        return view('./admin_modulos/registrareditor');
    });


    Route::get('asignarEditor/{id}', 'OrdeneController@edit');
    Route::get('asignareditor/{id}', 'UsuarioController@indexE');
    Route::post('actualizarOrden/{id}', 'OrdeneController@update');
    Route::get('verOrden/{id}', 'OrdeneController@indexV');
    Route::post('eliminarOrden/{id}', 'OrdeneController@destroy');
    Route::post('eliminarArtista{id}', 'UsuarioController@destroy');
    Route::post('insertarEditor', 'UsuarioController@RegistroEditor');
    Route::post('actualizarEditor/{id}', 'UsuarioController@update');
    Route::get('editarEditor/{id}', 'UsuarioController@edit');
    Route::get('meditor', 'UsuarioController@index');
    Route::post('guardarorden', 'OrdeneController@store');
    Route::get('cliente', 'ProductoController@index');
    Route::get('mcliente','OrdeneController@index');
    Route::get('verOrdenC','OrdeneController@indexO');
    Route::get('editor','UsuarioController@index');

});
